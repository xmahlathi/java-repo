/**
 * Created by Admin on 10/7/2015.
 */
import java.util.Scanner;
public class KnuthMorris {
  public static  int search(String pattern, String text) {
        int[] lsp = computeLspTable(pattern);

        int j = 0;  // Number of chars matched in pattern
        for (int i = 0; i < text.length(); i++) {
            while (j > 0 && text.charAt(i) != pattern.charAt(j)) {
                // Fall back in the pattern
                j = lsp[j - 1];  // Strictly decreasing
            }
            if (text.charAt(i) == pattern.charAt(j)) {
                // Next char matched, increment position
                j++;
                if (j == pattern.length())
                    return i - (j - 1);
            }
        }
        return -1;  // Not found
    }
 public static  int[] computeLspTable(String pattern) {
        int[] lsp = new int[pattern.length()];
        lsp[0] = 0;  // Base case
        for (int i = 1; i < pattern.length(); i++) {
            // Start by assuming we're extending the previous LSP
            int j = lsp[i - 1];
            while (j > 0 && pattern.charAt(i) != pattern.charAt(j))
                j = lsp[j - 1];
            if (pattern.charAt(i) == pattern.charAt(j))
                j++;
            lsp[i] = j;
        }
        return lsp;
    }

    public static void main(String args[]) {

        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter word:");
        String word=scanner.nextLine();
        System.out.println("Enter Pattern:");
        String pattern=scanner.nextLine();
        int answer=search(pattern,word);
        int [] table= computeLspTable(pattern);
        for(int tableElements=0;tableElements<table.length;tableElements++)
        {
            System.out.println(table[tableElements]);
        }
    }

}
