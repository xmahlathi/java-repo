import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Admin on 9/18/2015.
 */
public class Anagrams {
    public static void main(String[] args) {
        try {
            URL myUrl = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
            InputStreamReader reader = new InputStreamReader(myUrl.openStream());
            BufferedReader br = new BufferedReader(reader);

            String inputString = null;
            System.out.println("Please enter word : ");
            Scanner scan = new Scanner(System.in);

            String word = scan.next();
            int count = 0;
            while ((inputString = br.readLine()) != null) {
                if (word.length() == inputString.length()) {
                    String inputword = sortCharacter(word);
                    String anotherString = sortCharacter(inputString);

                    if (inputword.equalsIgnoreCase(anotherString)) {
                        System.out.println(inputString);
                    }

                }
            }
        } catch (IOException ioe) {
            System.out.println("Error  : " + ioe);
        }
    }

    public static String sortCharacter(String value) {

        char[] charArray = value.toCharArray();
        Arrays.sort(charArray);
        String valueToReturn = String.valueOf(charArray);
        return valueToReturn;
    }
}
