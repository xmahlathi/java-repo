/**
 * Created by Admin on 10/8/2015.
 */
import java.util.*;
public class PanCake {
    public static void main (String args[])  // entry point from OS
    {
        Scanner s, ls;
        int pancakes[] = new int[300];
        int k;

        s = new Scanner(System.in);  // create new input Scanner

        while(s.hasNextLine())    /*  While there's more data to process...  */
        {
              /*  Read the integers  */
            ls = new Scanner(s.nextLine());
            k = 0;
            while (ls.hasNextInt())
                pancakes[k++] = ls.nextInt();

            processStack(pancakes, k);
        }
    }  /*  end of "main" procedure  */
    public static void processStack(int pancakes[], int L)
    {
        int bottom, max_so_far, index, i, j, k;

             /*  Print out the stack again  */
        for (j = 0; j < L; j++)
            System.out.print(pancakes[j] + " ");
        System.out.println();

        System.out.print("Flip sequence: ");

        for (bottom = L-1; bottom > 0; bottom--)
        {

                     /*  First, find a maximum of the
                         substack of elements
                              pancakes[0], pancakes[bottom]
                         Note that if this is duplicated, I am
                         finding the one closest to the bottom
                         of the substack.
                     */
            index = bottom;
            max_so_far = pancakes[bottom];
            for (i = bottom - 1; i >= 0; i--)
                if (pancakes[i] > max_so_far)
                {
                    max_so_far = pancakes[i];
                    index = i;
                }
            if (index != bottom)
            {
                if (index != 0)
                    System.out.print( (index+1) + " ");

                System.out.print( (bottom+1) + " ");


                int temp[] = new int[L];
                j = 0;
                for (i = bottom; i > index; i--, j++)
                    temp[j] = pancakes[i];
                for (i = 0; i < index; i++, j++)
                    temp[j] = pancakes[i];
                temp[j++] = pancakes[index];

                          /*  Having constructed the new (top) portion,
                              insert it back into the array for the next
                              time through the loop.
                          */
                for (i = 0; i <= bottom; i++)
                    pancakes[i] = temp[i];
            }
        }
        System.out.println("0");
        System.out.println();

        return;

    }  /*  end of "processStack" procedure  */


}  /*  end of "Pancakes" program  */

