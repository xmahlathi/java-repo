/**
 * Created by Admin on 10/1/2015.
 */
public class MaxInArray {
    public static void main(String args[]) {
        int Temp = 0;
        int Array[] = {11, 22, 45, 89, 657, 90, 45, 87, 90};
        Temp = Array[0];
        for (int elementsInArray = 1; elementsInArray < Array.length; elementsInArray++) {
            if (Temp < Array[elementsInArray]) {
                Temp = Array[elementsInArray];
            }
        }
        System.out.print("Max In Array : " + Temp);

    }
}
