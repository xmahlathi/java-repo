/**
 * Created by Admin on 9/21/2015.
 */

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ReadTextFile {
    public static String fileName = "data";

    public static ArrayList list = new ArrayList();

    public static void main(String args[]) {

        int sum = 0;
        try {
            ArrayList<String> Temp = readfile(list);
            String[] numbers = Temp.toArray(new String[Temp.size()]);

             for (int x = 0; x < numbers.length; x++) {

                   if(x==numbers.length-1) {
                       System.out.print(numbers[x] + "  ");
                       sum = sum + Integer.parseInt(numbers[x]);

                   }
                else
                   {
                       System.out.print(numbers[x] + " + ");
                   }
            }
        } catch (IOException oie) {

            System.out.println(oie);
        } finally {
            System.out.print(" = " + sum);
        }

    }

    public static ArrayList<String> readfile(ArrayList list) throws IOException {

        Scanner scan = new Scanner(new File("Data"));

        list = new ArrayList<String>();

        while (scan.hasNextLine()) {
            String line = scan.nextLine();

            Scanner scanner = new Scanner(line);
            scanner.useDelimiter(",");
            while (scanner.hasNext()) {
                list.add(scanner.next());
            }
            scanner.close();
        }

        scan.close();

        // System.out.println(list);
        return list;
    }

}

