/**
 * Created by Admin on 10/2/2015.
 */
import java.io.File;

public class FileDirectory {


        public static void main(String[] args) {
            walkin(new File("C:/Users/Admin/Documents/fundamentalsJava/java-repo/src")); //Replace this with a suitable directory
        }

    
        public static void walkin(File dir) {
            String pattern = ".txt";

            File listFile[] = dir.listFiles();
            if (listFile != null) {
                for (int i=0; i<listFile.length; i++) {
                    if (listFile[i].isDirectory()) {
                        walkin(listFile[i]);
                    } else {
                        if (listFile[i].getName().endsWith(pattern)) {
                            System.out.println(listFile[i].getPath());
                        }
                    }
                }
            }
        }
    }

