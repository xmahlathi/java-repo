/**
 * Created by Admin on 9/22/2015.
 */
public class BottlesProgram {
    public static void main(String args[]) {
        int TotBottles = 99;
        for (int x = 0; x < 99; x++) {
            System.out.println(TotBottles + "bottles on the wall");
            System.out.println(TotBottles + "bottles of beer");
            System.out.println("Take one down, pass it around");
            TotBottles--;
            if (TotBottles < 2) {
                System.out.println(TotBottles + " bottle ");
            }
            else {
                System.out.println(TotBottles + "bottles ");
            }
        }
    }
}
