/**
 * Created by Admin on 9/22/2015.
 */
public class AligningColumns {
    public static void main(String[] args) {
        String[] information = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line",
                "are$delineated$by$a$single$'dollar'$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                "column$are$separated$by$at$least$one$space.",
                "Further,$allow$for$each$word$in$a$column$to$be$either$left",
                "justified,$right$justified,$or$center$justified$within$its$column."};

        String[] splitedInfo = null;
        String message = "";
        System.out.println("left justified column");
        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String i : splitedInfo) {
                System.out.printf("%-15s", i);

            }
            System.out.println();

        }

        System.out.println("\nRight justified column");
        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String j : splitedInfo) {
                System.out.printf("%15s", j);

            }
            System.out.println();

        }


        System.out.println("\nCenter justified column");

        for (String info : information) {
            splitedInfo = info.split("\\$");

            for (String k : splitedInfo) {
                int space = k.length() / 2;
                System.out.printf("%s", k);


            }

            System.out.println();
        }


    }

}
