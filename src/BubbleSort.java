/**
 * Created by Admin on 9/22/2015.
 */
public class BubbleSort {
    public static int[] num = {6, 8, 0, 3, 5, 7, 8};
    public static int intArray = num.length;

    public static void main(String args[]) {

        int SortedArray[] = BubbleSort(num);
        for (int numberElements = 0; numberElements < intArray; numberElements++) {
            System.out.println(SortedArray[numberElements]);
        }

    }

    public static int[] BubbleSort(int[] num) {
        int j;
        boolean flag = true;
        int temp;

        while (flag) {
            flag = false;
            for (j = 0; j < num.length - 1; j++) {
                if (num[j] < num[j + 1])
                {
                    temp = num[j];
                    num[j] = num[j + 1];
                    num[j + 1] = temp;
                    flag = true;

                }
                ;
            }

        }
        return num;
    }

}

