/**
 * Created by Admin on 10/7/2015.
 */
public class RbgCalculator {
    public static void main(String[] args)
    {
        int r =128 ,g =192 ,b =79;
        System.out.print("#" + toBrowserHexValue(r) + toBrowserHexValue(g) + toBrowserHexValue(b));
    }

    private static String toBrowserHexValue ( int number){
        StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
        while (builder.length() < 2) {
            builder.append("0");
        }
        return builder.toString().toUpperCase();
    }

}


