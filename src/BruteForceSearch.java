/**
 * Created by Admin on 9/22/2015.
 */

import java.util.*;

public class BruteForceSearch {
    private char[] text;
    private char[] pattern;
    private int lengthOfText;
    private int pattern2;

    public void setString(String text , String pattern) {
        this.text = text.toCharArray();
        this.pattern = pattern.toCharArray();
        this.lengthOfText = text.length();
        this.pattern2= pattern.length();
    }

    public int search() {
        for (int outerloop = 0; outerloop < lengthOfText - pattern2; outerloop++) {
            int innerloop = 0;
            while (innerloop < pattern2 && text[outerloop + innerloop] == pattern[innerloop]) {
                innerloop++;
            }
            if (innerloop == pattern2) return outerloop;
        }
        return -1;
    }

    public static void main(String[] args) {
        BruteForceSearch bfs = new BruteForceSearch();
        System.out.println("Enter your text to be searched :");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println("Enter your pattern to use in order to search: ");
        String pattern = scanner.nextLine();

        bfs.setString(text, pattern);
        int first_occur_position = bfs.search();
        System.out.println("The text '" + pattern + "' is first found after the " + first_occur_position + " position.");


    }
}
