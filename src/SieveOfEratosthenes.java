/**
 * Created by Admin on 10/1/2015.
 */
import java.util.ArrayList;
public class SieveOfEratosthenes {
    public static long arguement= 15485863L ;


    public static void main(String args[])
    {
        sievFunction(arguement);
    }
    public static void sievFunction(long arguement)
    {
        ArrayList<Long> myPrimeList = new ArrayList<Long>();
        ArrayList<Long> myTempPrimeList = new ArrayList<Long>();
        ArrayList<Boolean> isPrimeNumber = new ArrayList<Boolean>();
        int index = 0;

        long maximum = arguement;
        long minimum = 2;

        do
        {
            myTempPrimeList.add(minimum);
            isPrimeNumber.add(true);
            minimum++;
        }while( minimum != (arguement+1));

        for(long i : myTempPrimeList)
        {
            if(isPrimeNumber.get(index))
            {
                myPrimeList.add(i);
                for(long j = i ; j*i <= maximum; j++)
                {
                    isPrimeNumber.set(myTempPrimeList.indexOf(j*i),false);
                }
            }
            index++;
        }

        System.out.println(myPrimeList);
    }
}
