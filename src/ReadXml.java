/**
 * Created by Admin on 9/21/2015.
 */

import java.io.*;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

p

public class ReadXml extends DefaultHandler {

     public static void main(String[] args) {

        try {
            File inputFile = new File("C:\\Users\\Admin\\Documents\\fundamentalsJava\\java-repo\\src\\Reading.xml");

            DocumentBuilderFactory create = DocumentBuilderFactory.newInstance();
            DocumentBuilder dCreate = create.newDocumentBuilder();
            Document doc = dCreate.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("Student");

            for (int temp = 0; temp < nList.getLength(); temp++){
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;
                    System.out.println( eElement.getAttribute("Name"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}